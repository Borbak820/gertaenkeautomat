#include <stdio.h>

int mainV3() {
    // Getraenke Automat Version 0.3

    float einwurf = 0;
    float betrag;
    int getraenk = 0;
    float menge = 0;
    float restbetrag = 0;

    printf("Getraenke Automat v0.3 \n"
           "|Automat gibt kein Wechselgeld| \n\n"
           "Bitte waehlen Sie Ihr Getraenk aus: \n"
           "1) Wasser (0.50 Euro) \n"
           "2) Limonade (1.00 Euro) \n"
           "3) Bier (2.00 Euro) \n"
           "Geben Sie 1,2 oder 3 ein.\n");
    scanf("%d", &getraenk);

    printf("Bitte Menge waehlen: \n");
    scanf("%f", &menge);


    switch (getraenk) {
        case 1:
            betrag = 1 * menge;
            break;

        case 2:
            betrag = 2 * menge;
            break;

        case 3:
            betrag = 3 * menge;
            break;


        default:
            printf("Keine gueltige Auswahl getroffen.");
            return 0;
    }
    do {
        printf("Bitte werfen Sie %.2f Euro ein: \n", betrag);
        scanf("%f", &einwurf);
        betrag = betrag - einwurf;
    } while (betrag > 0);
    switch (getraenk) {

        case 1:
            printf("Viele Dank, geniessen Sie Ihr Wasser \n");
            break;

        case 2:
            printf("Viele Dank, geniessen Sie Ihre Limonade \n");
            break;

        case 3:
            printf("Viele Dank, geniessen Sie Ihr Bier \n");
            break;

    }
    }
